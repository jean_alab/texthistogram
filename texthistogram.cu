#include <fstream>
#include <iostream>
#include <cuda.h>

using namespace std;


__global__ void fillTextHistogramGPU(char *fileData, int *histogram, size_t fileSize);
//void fillTextHistogramCPU(char *fileData, int *histogram, size_t fileSize);
void writeTextHistogramCSV(int *histogram, ofstream *histFileHandle);
int main(int argc, char **argv)
{
	if(argc!=3)
	{
		printf("[ERR] Usage: %s textFile.txt histogramFile.txt\n",argv[0]);
		exit(0);
	}
	ifstream dataFileHandle;
	dataFileHandle.open(argv[1],ifstream::ate);
	if(!dataFileHandle.is_open())
	{
		printf("[ERR] Couldn't open file %s for reading\n",argv[1]);
		exit(0);
	}
	ofstream histFileHandle;
	histFileHandle.open(argv[2]);
	if(!histFileHandle.is_open())
	{
		printf("[ERR] Couldn't open file %s for writing\n",argv[2]);
		exit(0);
	}
	size_t fileSize = dataFileHandle.tellg();
	dataFileHandle.seekg(0,dataFileHandle.beg);
	printf("[INFO] File size: %lu bytes\n",fileSize);
	char *h_fileData;
	char *d_fileData;
	h_fileData = (char *)malloc(sizeof(char)*fileSize);


	cudaMalloc((void **)&d_fileData,sizeof(char)*fileSize);

	dataFileHandle.read(h_fileData,fileSize);
	int bins = 'z'-'a'+1;
	int *h_histogram;
	int *d_histogram;

	h_histogram = (int *)malloc(sizeof(int)*bins);
	cudaMalloc((void **)&d_histogram,sizeof(int)*bins);
	memset(h_histogram,0,sizeof(int)*bins);
	cudaMemset(d_histogram,0,sizeof(int)*bins);

	cudaMemcpy(d_fileData,h_fileData,sizeof(char)*fileSize,cudaMemcpyHostToDevice);

	dim3 blockDim(32);
	dim3 gridDim(1024);
//	dim3 gridDim(ceil((int)bins/32));

	fillTextHistogramGPU<<<gridDim,blockDim>>>(d_fileData,d_histogram,fileSize);
	cudaDeviceSynchronize();
		cudaMemcpy(h_histogram,d_histogram,sizeof(int)*bins,cudaMemcpyDeviceToHost);
	//fillTextHistogramCPU(fileData,h_histogram,fileSize);
	writeTextHistogramCSV(h_histogram,&histFileHandle);
	dataFileHandle.close();
	histFileHandle.close();
	return 0;
}
/*void fillTextHistogramCPU(char *h_fileData, int *histogram, size_t fileSize)
{
	for(size_t idx=0;idx<fileSize;idx++)
	{
		char currentCharacter = tolower(fileData[idx]);
		if(currentCharacter>'z'|| currentCharacter<'a') continue;
		histogram[currentCharacter-'a']++;
	}
}
*/
__global__ void fillTextHistogramGPU(char *fileData, int *histogram, size_t fileSize)
{

	unsigned int tid=threadIdx.x + blockIdx.x * blockDim.x;


	for(size_t idx=tid;idx<fileSize;idx += blockDim.x*gridDim.x)
	{
		//char currentCharacter = tolower(fileData[idx]);
		char currentCharacter = fileData[idx];
		if(currentCharacter>'z'|| currentCharacter<'a')continue; atomicAdd(&histogram[currentCharacter-'a'],1);
		//histogram[currentCharacter-'a']++;
	}
}

void writeTextHistogramCSV(int *histogram, ofstream *histFileHandle)
{
	*histFileHandle << "letter,freq" << endl;
	int binsNumber = 'z'-'a'+1;
	float persentege[26]={0};
	int total=0,aux;

	for(int i=0;i<binsNumber;i++)
	{
		*histFileHandle << (char)('a'+i) << "," << histogram[i] << endl;
		aux=total;
		total=histogram[i]+aux;
	}
	*histFileHandle << "language: " << endl;
	for(int i=0;i<binsNumber;i++)
		persentege[i]=(histogram[i]*100.0)/total;
	if(persentege[0]>8.0 && persentege[0]<10.0)
		*histFileHandle << "English " << endl;
	else
			*histFileHandle << "Spanish" << endl;

	for(int i=0;i<binsNumber;i++)
		*histFileHandle << (char)('a'+i) << "," << persentege[i] << endl;


}
